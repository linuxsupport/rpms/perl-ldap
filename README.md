# perl-LDAP

* This package is provided by EPEL, however we package it and store it in the
CERN repository to ensure that CERN tools that depend on it
(lpadmincern, phonebook, etc) can be installed without requiring having EPEL
installed

**Specfile has been modified to avoid installing build deps for certain tests, since `openldap-servers` rpm is no longer available for C(S)8.**

Ref. https://bugs.centos.org/view.php?id=16959, https://access.redhat.com/solutions/2440481
